### D-Day Schedule - Sunday, December 1st 
* **Announce on Slack**
  * time: 8am
  * message: Today the GitLab Blog is deploying from a new repo! This also means there is a content lock on blog posts from 9am till 1pm today. This will ensure that all content and commit history is properly synced. After the GitLab Blog deploys, you will edit, create, and merge all blog posts from the repo linked below. I'll let everyone know here once deployment is completed and the content lock is lifted.
* **Lock blog content**
  * time 9am - 1pm (just an estimate, will circle back)
  * A content lock will need to be placed on blog posts during the launch. 
* **Pull blog post git commit history**
* * time 9am - 10pm 
```sh
cd www-gitlab-com`
git fetch
git merge origin/master
git subtree split -P source/posts -b blog
mkdir blog-sutree
pushd blog-sutree
git init
git pull /Users/laurenbarker/Documents/htdocs/filter-branch-demo/www-gitlab-com blog
git remote add origin git@gitlab.com:gitlab-com/blog.git
git merge -Xours origin/master --allow-unrelated-histories
```

* **Initiate deployment of blog**
  * time: 10:15am - 11am
  * what: deploy https://gitlab.com/gitlab-com/blog/ to about.gitlab.com
  * This should just involve submitting a MR that addes GCP to our gitlab-ci file. 
* **confirm content deployment**
  * time: 11am-11:30am
  * what: confirm that contents of blog repo have been deployed to about.gitlab.com
  * Add comment to blog posts that will allow us to verify deployment
* **Remove blog from www-gitlab-com**
  * time: 11:30am - 12:30pm
  * what: Merge https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34973, removing blog content from repo
* **Confirm blog content has been removed from www-gitlab-com**
  * time: 12:35pm
  * what: Confirm that blog content has been removed from www-gitlab-com and isn't being deployed.
* **Announce completion on slack**
  * time: 1pm
  * message: tbd