require 'extensions/breadcrumbs'
require 'extensions/partial_build'
require 'lib/homepage'
require 'lib/mermaid'
require 'lib/plantuml'

# Per-page layout changes:
#
# With no layout

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  ugly: true,
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: "GFM"

activate :blog do |blog|
  blog.sources = '/posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '/blog/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/blog/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Development-specific configuration
configure :development do
  set :js_dir, '//127.0.0.1:4567/javascripts'
  set :css_dir, '//127.0.0.1:4567/stylesheets'
  set :images_dir, '//127.0.0.1:4567/images'
  # port
  set(:port, 5678)
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  set :js_dir, '//about.gitlab.com/javascripts'
  set :css_dir, '//about.gitlab.com/stylesheets'
  set :images_dir, '//about.gitlab.com/images'
  activate :minify_html
end

page '/404.html', directory_index: false

ignore '/direction/template.html'
ignore '/includes/*'
ignore '/releases/template.html'
ignore '/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
